<?php

/**
 * @file
 * Contains the administration pages for Custom Prepro.
 */

/**
 * Form for Custom Prepro module settings.
 */
function custom_prepro_settings_form($form, &$form_state) {

  $form['custom_prepro_flush'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#value' => t('Click this button to flag all Custom Prepro files for regeneration.'),
  );

  $form['custom_prepro_flush']['flush'] = array(
    '#type' => 'submit',
    '#submit' => array('_flush_custom_prepro'),
    '#value' => t('Flush Custom Prepro files'),
  );

  $libraries = libraries_info();

  $custom_prepro_engines = array();

  foreach ($libraries as $key => $library) {
    if (isset($library['custom prepro callback'])) {
      $custom_prepro_engines[] = libraries_detect($key);
    }
  }

  $custom_prepro_engine_element = array(
    '#type' => 'radios',
    '#title' => t('Custom CSS preprocesor engine'),
    '#options' => array(),
    '#required' => TRUE,
    '#default_value' => variable_get('custom_prepro_engine', ''),
  );

  foreach ($custom_prepro_engines as $custom_prepro_engine) {
    $custom_prepro_engine_element['#options'][$custom_prepro_engine['machine name']] = $custom_prepro_engine['name'];

    $custom_prepro_engine_element[$custom_prepro_engine['machine name']] = array(
      '#type' => 'radio',
      '#title' => t('@engine_name - <a href="@vendor_url">@vendor_url</a>', array('@engine_name' => $custom_prepro_engine['name'], '@vendor_url' => $custom_prepro_engine['vendor url'])),
      '#return_value' => $custom_prepro_engine['machine name'],
      '#description' => t('Missing - Click vendor link above to read installation instructions.'),
      '#disabled' => empty($custom_prepro_engine['installed']),
    );

    if ($custom_prepro_engine['installed']) {
      $custom_prepro_engine_element[$custom_prepro_engine['machine name']]['#description'] = t('v%version Installed', array('%version' => $custom_prepro_engine['version']));
    }
  }

  $form['custom_prepro_engine'] = $custom_prepro_engine_element;

  $form['developer_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Developer Options'),
    '#collapsible' => TRUE,
    '#collapsed' => !(variable_get(CUSTOM_PREPRO_DEVEL, FALSE)),
  );

  $form['developer_options'][CUSTOM_PREPRO_DEVEL] = array(
    '#type' => 'checkbox',
    '#title' => t('Developer Mode'),
    '#description' => t('Enable developer mode to ensure css files are regenerated every page load.'),
    '#default_value' => variable_get(CUSTOM_PREPRO_DEVEL, FALSE),
  );

  $form['#submit'] = array('custom_prepro_settings_form_submit');

  return system_settings_form($form);
}

/**
 * Form submission function.
 *
 * Trigger clear of Custom Prepro module cache data.
 */
function custom_prepro_settings_form_submit($form, &$form_state) {
  cache_clear_all('custom_prepro:', 'cache', TRUE);
}

/**
 * Submit handler for cache clear button.
 */
function _flush_custom_prepro($form, &$form_state) {
  custom_prepro_flush_caches();

  drupal_set_message(t('Custom Prepro files cache cleared.'), 'status');
}
