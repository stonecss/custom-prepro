<?php

/**
 * @file
 *   Contains functions related to compiling .css files.
 */
function _custom_prepro_output_path(&$item) {
  $input_file = $item['data'];
  $item['custom_prepro']['input_file'] = $input_file;

  $output_path_array = array(
    '!custom_prepro_output_dir' => _custom_prepro_get_dir(),
    // Strip '.css' extension of filenames following the RTL extension pattern.
    '!input_file_basename' => basename(basename($input_file, '.css'), '.css'),
  );

  $output_path = strtr('public://custom_prepro/!custom_prepro_output_dir/!input_file_basename.css', $output_path_array);

  // Ensure the destination directory exists.
  if (_custom_prepro_ensure_directory(dirname($output_path))) {
    $item['custom_prepro']['output_file'] = $output_path;
  }
}

function _custom_prepro_check_build(&$item) {
  $input_file = $item['custom_prepro']['input_file'];

  $build_required = ((bool) variable_get(CUSTOM_PREPRO_DEVEL, FALSE));

  // Set $rebuild if this file or its children have been modified.
  if (($custom_prepro_file_cache = cache_get('custom_prepro:devel:' . drupal_hash_base64($input_file)))) {
    // Iterate over each file and check if there are any changes.
    foreach ($custom_prepro_file_cache->data as $filepath => $filemtime) {
      // Only rebuild if there has been a change to a file.
      if (is_file($filepath) && filemtime($filepath) > $filemtime) {
        $build_required = TRUE;
        break;
      }
    }
  }
  else {
    // No cache data, force a rebuild for later comparison.
    $build_required = TRUE;
  }

  $item['custom_prepro']['build_required'] = $build_required;
}


function _custom_prepro_process_file(&$item) {
  // $output_file doesn't exist or is flagged for build.
  if (!is_file($item['custom_prepro']['output_file'])
    || !empty($item['custom_prepro']['build_required'])
  ) {
    $output_data = NULL;
    $error = NULL;

    if ($callback = _custom_prepro_inc()) {
      list($output_data, $error) = call_user_func_array($callback, array($item['data']));
    }
    else {
      $error = t('No engine found.');
    }

    if (!empty($error)) {
      $message_vars = array('@message' => $error, '%input_file' => $item['data']);

      watchdog('Custom Prepro', 'Custom Prepro error: @message, %input_file', $message_vars, WATCHDOG_ERROR);
    }

    if (isset($output_data)) {
      // Fix paths for images as .css is in different location.
      $output_data = _custom_prepro_rewrite_paths($item['custom_prepro']['input_file'], $output_data);
      file_unmanaged_save_data($output_data, $item['custom_prepro']['output_file'], FILE_EXISTS_REPLACE);
    }
  }

  if (is_file($item['custom_prepro']['output_file'])) {
    // Set render path of the stylesheet to the compiled output.
    $item['data'] = $item['custom_prepro']['output_file'];
  }
}

/**
 * Extract URLs from CSS text.
 *
 * Taken from: http://nadeausoftware.com/articles/2008/01/php_tip_how_extract_urls_css_file
 * See website for usage
 */
function _custom_prepro_extract_css_urls( $text ) {
  $urls = array( );

  $url_pattern     = '(([^\\\\\'", \(\)]*(\\\\.)?)+)';
  $urlfunc_pattern = 'url\(\s*[\'"]?' . $url_pattern . '[\'"]?\s*\)';
  $pattern         = '/(' .
       '(@import\s*[\'"]' . $url_pattern     . '[\'"])' .
      '|(@import\s*'      . $urlfunc_pattern . ')'      .
      '|('                . $urlfunc_pattern . ')'      .  ')/iu';
  if ( !preg_match_all( $pattern, $text, $matches ) )
      return $urls;

  // @import '...'
  // @import "..."
  foreach ( $matches[3] as $match )
      if ( !empty($match) )
          $urls['import'][] =
              preg_replace( '/\\\\(.)/u', '\\1', $match );

  // @import url(...)
  // @import url('...')
  // @import url("...")
  foreach ( $matches[7] as $match )
      if ( !empty($match) )
          $urls['import'][] =
              preg_replace( '/\\\\(.)/u', '\\1', $match );

  // url(...)
  // url('...')
  // url("...")
  foreach ( $matches[11] as $match )
      if ( !empty($match) )
          $urls['property'][] =
              preg_replace( '/\\\\(.)/u', '\\1', $match );

  return $urls;
}

/**
 * Scrape and find dependencies in a given css file.
 *
 * @param string $input_file
 *   Path of source file.
 * @param array $dependencies
 *   Array of files that are @import'ed in $input_file, recursively.
 */
function _custom_prepro_find_dependencies($input_file, &$dependencies = array()) {
  $cwd = getcwd();

  if (is_file($input_file) && ($text = file_get_contents($input_file))) {
    $results = _custom_prepro_extract_css_urls($text);

    $parent_directory = dirname($input_file);
    chdir($parent_directory);

    if (!empty($results['import'])) {
      foreach ($results['import'] as $import_file) {
        if (substr($import_file, -4) != '.css') {
          $import_file .= '.css';
        }

        if (is_file($import_file)) {
          $dependencies[] = drupal_realpath($import_file);

          _custom_prepro_find_dependencies($import_file, $dependencies);
        }
      }
    }
  }

  chdir($cwd);
}

/**
 * Normalize keeping track of changed files.
 *
 * @param string $input_file
 *   Path of source file.
 * @param array $dependencies
 *   Array of files that are @import'ed in $input_file, recursively.
 */
function _custom_prepro_cache_dependencies($input_file, $dependencies = array()) {
  $watched_files = array();

  foreach ($dependencies as $dependency) {
    // Full path on file should enforce uniqueness in associative array.
    $watched_files[drupal_realpath($dependency)] = filemtime($dependency);
  }

  cache_set('custom_prepro:devel:' . drupal_hash_base64($input_file), $watched_files);
}

function _custom_prepro_store_cache_info(&$item) {
  // Only match when output_file exists.
  if ($item['data'] === $item['custom_prepro']['output_file']) {
    $custom_prepro_watch_cache = $item;

    $custom_prepro_watch_cache['data'] = $item['custom_prepro']['input_file'];

    cache_set('custom_prepro:watch:' . drupal_hash_base64(file_create_url($item['custom_prepro']['output_file'])), $custom_prepro_watch_cache);

    // (?) 'preprocess' being FALSE generates a discreet <link /> rather than an @import.
    $item['preprocess'] = FALSE;
  }
}

/**
 * Copied functionality from drupal_build_css_cache() for our own purposes.
 *
 * This function processes $contents and rewrites relative paths to be absolute
 * from web root. This is mainly used to ensure that compiled .css files still
 * reference images at their original paths.
 *
 * @return string
 *   Processed styles with replaced paths.
 *
 * @see drupal_build_css_cache()
 */
function _custom_prepro_rewrite_paths($input_filepath, $contents) {
  $output = '';

  // Build the base URL of this CSS file: start with the full URL.
  $css_base_url = file_create_url($input_filepath);
  // Move to the parent.
  $css_base_url = substr($css_base_url, 0, strrpos($css_base_url, '/'));
  // Simplify to a relative URL if the stylesheet URL starts with the
  // base URL of the website.
  if (substr($css_base_url, 0, strlen($GLOBALS['base_root'])) == $GLOBALS['base_root']) {
    $css_base_url = substr($css_base_url, strlen($GLOBALS['base_root']));
  }

  _drupal_build_css_path(NULL, $css_base_url . '/');
  // Anchor all paths in the CSS with its base URL, ignoring external and absolute paths.
  $output .= preg_replace_callback('/url\(\s*[\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\s*\)/i', '_drupal_build_css_path', $contents);

  return $output;
}

