<?php

/**
 * @file
 * Contains Drush related functions.
 */

/**
 * Implements hook_drush_cache_clear().
 *
 * This adds an option on drush 'cache-clear'.
 */
function custom_prepro_drush_cache_clear(&$types) {
  $types['custom_prepro'] = 'custom_prepro_flush_caches';
}
